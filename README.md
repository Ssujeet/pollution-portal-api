# Pollution Portal API

An api of Pollution portal website

# Dependencies

Redis(A message proper)
Install redis package in system and enable the service
```
systemctl start redis.service
```

Make a virtual Environment and install those package
1. Package
2. redis
3. flask
4. celery
5. beautifulsoup
6. selenuim
7. requests



Run the program
```
export export FLASK_APP=task.py
celery -A task.celery beat
celery -A task.celery worker --loglevel=info
flask run

```